# A quoi sert ce repo ?

Ce repo sert à présenter quelques outils pratiques pour le développement en golang.

# Quels outils sont présentés ?

Les outils présentés ici sont :
- [**github.com/gorilla/mux**](https://github.com/gorilla/mux) : Permet de faciliter la création d'API en gérant le routage de façon plus performante que le package natif net/http ✔
> Note : Gorilla/mux ne sera peut-être plus nécessaire après go 1.22. [Voir ici pour plus d'infos](https://eli.thegreenplace.net/2023/better-http-server-routing-in-go-122/)
- [**github.com/rs/zerolog**](https://github.com/rs/zerolog) : Gère les logs de façon performante et nativement au format JSON ✔
- [**github.com/stretchr/testify**](https://github.com/stretchr/testify) : Facilite l'écriture de tests avec des assertions notamment ✔
- [**github.com/spf13/viper**](https://github.com/spf13/viper) : Facilite la gestion des paramètres via des flags, fichiers de configuration, variables d'environnement, etc ✔
- [**github.com/spf13/cobra**](https://github.com/spf13/cobra) : Facilite la création de cli en go, avec notamment une gestion des flags. S'intègre avec viper. ✔
- [**go.opentelemetry.io/otel**](https://github.com/open-telemetry/opentelemetry-go) : Permet d'implémenter et exposer des métriques et traces dans la norme OpenTelemetry ✔
- [**VSCode devcontainer**](https://code.visualstudio.com/docs/devcontainers/containers) : *Non spécifique à go*. Permet de développer dans environnement de développement prédéfinit, dans un container ✔
- [**Air**](https://github.com/cosmtrek/air) : Permet de fluidifier le développement avec du hot reload ✔

# Informations supplémentaires

Le repo utilise le layout [https://github.com/golang-standards/project-layout](https://github.com/golang-standards/project-layout)
