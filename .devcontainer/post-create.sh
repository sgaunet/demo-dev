go version

go install github.com/cosmtrek/air@latest

mkdir -p logs

cat .devcontainer/demo-dev.source >> ~/.zshrc

echo "
autoload -Uz compinit; compinit;
bindkey "^Xa" _expand_alias
zstyle ':completion:*' completer _expand_alias _complete _ignored
zstyle ':completion:*' regular true
" >> ~/.zshrc
