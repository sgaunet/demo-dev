# 1/ Devcontainer

Voir fichier [.devcontainer/devcontainer.json](../.devcontainer/devcontainer.json)

# 2/ Air

Voir fichier [.air.toml](../.air.toml)

# 3/ Zerolog
- [Initialisation de zerolog](../cmd/techrexapi/main.go#L80)
- [Exemple d'utilisation d'un log](../internal/controller/techrex_controller.go#L62)
- [Retrouver le fichier de logs](../logs/techRex.log)

# 4/ Testify

## Testify vs lib go / Assert vs Require
- [Fichier ici](../test/demo_test.go)

## Mock
- [Définition d'une interface](../internal/service/image_service.go#L12)
- [Implémentation de l'interface](../internal/controller/techrex_controller_test.go#L13)
- [Utilisation du mock](../internal/controller/techrex_controller_test.go#L36)

## Suite

- [Définition d'une suite](../internal/service/image_service_test.go#L11)
- [Setup suite](../internal/service/image_service_test.go#L16)

# 5/ Cobra + Viper
- [Définition d'une commande Cobra](../cmd/techrexapi/main.go#L23)
- [Définition de flags Cobra + Mapping Viper](../cmd/techrexapi/main.go#L66)
- [Exemple d'utilisation d'un flag Viper](../cmd/techrexapi/main.go#L84)

# 6/ Gorilla Mux
MUX = HTTP request multiplexer
- [Définition des routes](../cmd/techrexapi/main.go#L114)

# 7/ Open Telemetry
- [Mise en place des ressources, provider, propagator](../internal/service/opentelemetry_service.go)
- [Définition de l'exporteur prometheus](../internal/service/opentelemetry_service.go#L43)
- [Définition des métriques HTTP par défaut](../cmd/techrexapi/main.go#L121)
- [Définition d'une métrique custom](../internal/controller/techrex_controller.go#L31)
- [Utilisation d'une métrique custom](../internal/controller/techrex_controller.go#L74)
