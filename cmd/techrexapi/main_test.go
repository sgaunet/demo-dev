package main

import (
	"go-dev/internal/controller"
	"go-dev/internal/service"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestRequestHandler(t *testing.T) {
	expected := "image/png"
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	w := httptest.NewRecorder()
	imageService := &service.ImageServiceImpl{}
	techRexController := controller.NewTechRexController(imageService)
	helloServer := techRexController.GetTechRexRandomImageHandler()
	helloServer(w, req)
	res := w.Result()
	defer res.Body.Close()

	_, err := io.ReadAll(res.Body)

	if err != nil {
		t.Errorf("Error: %v", err)
	}

	if string(res.Header.Get("Content-Type")) != expected {
		t.Errorf("Expected header Content-Type to be %v but was %v", expected, res.Header.Get("Content-Type"))
	}

}

func BenchmarkHelloServer(b *testing.B) {
	for n := 0; n < b.N; n++ {
		req := httptest.NewRequest(http.MethodGet, "/", nil)
		w := httptest.NewRecorder()
		imageService := &service.ImageServiceImpl{}
		techRexController := controller.NewTechRexController(imageService)
		helloServer := techRexController.GetTechRexRandomImageHandler()
		helloServer(w, req)
		res := w.Result()
		defer res.Body.Close()
	}
}
