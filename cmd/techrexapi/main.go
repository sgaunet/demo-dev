package main

import (
	"go-dev/internal/controller"
	"go-dev/internal/service"
	"io"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/rs/zerolog/pkgerrors"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
)

// Commande principale
var rootCmd = &cobra.Command{
	Short: "Display informations about the server",
	Long:  `This command will display some informations about the server`,
	Run: func(cmd *cobra.Command, args []string) {
		log.Info().Msgf("%s", args)
		log.Info().Msg("Go application used for demo purpose, with a tech rex flavor")
		log.Debug().Msg("wondering if it works...")
	},
}

func main() {
	// Définition de 2 sous-commandes
	helloWorldCmd := &cobra.Command{
		Use:   "hello",
		Short: "Print hello world",
		Long:  `This command will print hello world for demo purpose`,
		Run: func(cmd *cobra.Command, args []string) {
			log.Info().Msg("Hello DevopsDDay !")
		},
	}
	rootCmd.AddCommand(helloWorldCmd)

	startServerCmd := &cobra.Command{
		Use:   "start",
		Short: "Start the server",
		Long:  `This command will start the server with some additionnal configuration`,
		Run: func(cmd *cobra.Command, args []string) {
			setupLogger()

			startupHttpServer()
		},
	}
	rootCmd.AddCommand(startServerCmd)

	service.SetupOpentelemetryService()

	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

// Fonction exécutée avant le reste. On y définit les flags via Viper + Cobra
func init() { // Définition des flags. Persistent = valable pour toutes les subcommands
	rootCmd.PersistentFlags().StringP("log-level", "l", "INFO", "Define log level")
	rootCmd.PersistentFlags().String("log-path", "/workspaces/go-dev/logs/techrex.log", "Define log file path")
	rootCmd.PersistentFlags().StringP("img-path", "i", "/workspaces/go-dev/assets/images", "Define path to the images folder")

	// Binding entre viper et cobra
	viper.BindPFlags(rootCmd.PersistentFlags())

	// Conversion des "-" des flags en "_" dans les variables d'environnement
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
	// Création automatique de variables d'environnement pour tous les flags=
	viper.AutomaticEnv()
}

func setupLogger() {
	// Affichage des stacks d'erreur complètes
	zerolog.ErrorStackMarshaler = pkgerrors.MarshalStack

	log.Debug().Msgf("log-level flag is %s", viper.GetString("log-level"))
	// Définition du level des logs global, basé sur la conf viper
	if level, err := zerolog.ParseLevel(viper.GetString("log-level")); err != nil {
		log.Warn().Err(err).Msg("error parsing log level %s, resorting to info")
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	} else {
		zerolog.SetGlobalLevel(level)
	}
	log.Info().Msgf("initializing logger with level %s", zerolog.GlobalLevel())

	// Définition des différents writers
	// Un premier orienté développeur, avec des logs colorés et faciles à lire
	var logWriter io.Writer = zerolog.ConsoleWriter{Out: os.Stdout}
	// Un deuxième dans un fichier
	logFile, err := os.OpenFile(viper.GetString("log-path"), os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err == nil {
		log.Info().Msgf("will be logging to file %s", viper.GetString("log-path"))
		logWriter = zerolog.MultiLevelWriter(logWriter, logFile)
	} else {
		log.Err(err).Msgf("error open log file at %s", viper.GetString("log-path"))
	}
	log.Logger = zerolog.New(logWriter).With().Timestamp().Caller().Logger()
}

func startupHttpServer() {
	imageService := &service.ImageServiceImpl{}
	techRexController := controller.NewTechRexController(imageService)
	log.Info().Msg("Starting HTTP server on 8880...")

	// Création d'un routeur mux
	router := mux.NewRouter()
	// On peut définir à quelles méthodes on a accès (ici uniquement GET)
	router.HandleFunc("/techrex", techRexController.GetTechRexRandomImageHandler()).
		Methods("GET")
	router.HandleFunc("/techrex/{imageId}", techRexController.GetTechRexPreciseImageHandler()).
		Methods("GET")
	// Ici on définit une route /metrics pour exposer nos métriques prometheus
	router.Handle("/metrics", promhttp.Handler())

	// Ici on indique à OpenTelemetry de récupérer des métriques sur tous les endpoints
	otelHandler := otelhttp.NewHandler(router, "/")

	server := &http.Server{
		Addr:              ":8880",
		ReadHeaderTimeout: 3 * time.Second,
		Handler:           otelHandler,
	}

	errStart := false
	go func() {
		time.Sleep(time.Second * 3)
		if !errStart {
			log.Info().Msg("Server is up, you can now call http://localhost:8880")
		}
	}()

	if err := server.ListenAndServe(); err != nil {
		errStart = true
		log.Panic().Err(err).Msg("Error while starting the HTTP server")
	}
}
