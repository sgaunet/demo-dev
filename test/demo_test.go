package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestTestifySimpleAssert(t *testing.T) {
	// Ici les deux tests sont joués, une assertion qui fail laisse continuer le test
	assert.Equal(t, 13, 14, "Just a dumb test")
	assert.True(t, false, "This is another dumb test")
}

func TestTestifyRequire(t *testing.T) {
	// On utilise souvent require pour vérifier la non nullité avant un assert
	require.Equal(t, 13, 14, "Just a dumb test")
	// Ce test ne devrait pas être joué, parce que le require fail
	assert.True(t, false, "This is another dumb test")
}

func TestWithoutTestify(t *testing.T) {
	// Sans testify, on doit faire la gestion d'erreurs à la main.
	// C'est plus fastidieux et error prone
	if 13 != 14 {
		t.Errorf("Expected 13 but got %v", 14)
	}
}
