package controller

import (
	"go-dev/internal/service"
	"math/rand"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/metric"
)

type TechRexController struct {
	images               []string
	imagesFolder         string
	imageService         service.ImageService
	techRexMeter         metric.Meter
	imageReturnedCounter metric.Int64Counter
}

func NewTechRexController(imageService service.ImageService) *TechRexController {
	trc := &TechRexController{
		imageService: imageService,
		imagesFolder: viper.GetString("img-path"),
		techRexMeter: otel.Meter("techrex-meter"),
	}
	trc.imageReturnedCounter, _ = trc.techRexMeter.Int64Counter("imageReturned.counter",
		metric.WithDescription("How many times was this specific image index returned"),
	)
	trc.initImagesCache()
	return trc
}

func (trc *TechRexController) GetTechRexRandomImageHandler() func(w http.ResponseWriter, req *http.Request) {
	return trc.techRexRandomImageHandler
}

func (trc *TechRexController) GetTechRexPreciseImageHandler() func(w http.ResponseWriter, req *http.Request) {
	return trc.techRexPreciseImageHandler
}

func (trc *TechRexController) initImagesCache() {
	if len(trc.images) > 0 {
		log.Debug().Msg("Not reloading images, already present")
	} else {
		if imgs, err := trc.imageService.ReadImageNames(trc.imagesFolder); err != nil {
			log.Error().
				Stack().
				Err(err).
				Msg("Error while reading directory")
		} else {
			trc.images = imgs
		}
	}
}

func (trc *TechRexController) techRexRandomImageHandler(w http.ResponseWriter, req *http.Request) {
	log.Debug().Str("method", req.Method).Str("path", req.RequestURI).Msg("Received an HTTP call")

	var buf []byte
	var err error
	index := 0
	if len(trc.images) > 1 {
		index = rand.Intn(len(trc.images) - 1)
	}
	buf, err = trc.imageService.ReadImage(trc.imagesFolder, trc.images[index])
	if err != nil {
		log.Fatal().Err(err).Msg("Error while reading files")
	}
	trc.imageReturnedCounter.Add(req.Context(),
		1,
		metric.WithAttributes(attribute.Int("index", index)),
	)

	w.Header().Set("Content-Type", "image/png")
	w.Write(buf)
}

func (trc *TechRexController) techRexPreciseImageHandler(w http.ResponseWriter, req *http.Request) {
	log.Debug().Str("method", req.Method).Str("path", req.RequestURI).Msg("Received an HTTP call")

	imageIdStr := mux.Vars(req)["imageId"]

	imageId, err := strconv.Atoi(imageIdStr)
	if err != nil {
		log.Warn().Err(err).Msg("imageId should be a valid number")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if imageId > len(trc.images)-1 || imageId < 0 {
		log.Warn().Err(err).Msgf("imageId should be between 0 and %v", len(trc.images))
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	buf, err := trc.imageService.ReadImage(trc.imagesFolder, trc.images[imageId])
	if err != nil {
		log.Fatal().Err(err).Msg("Error while reading files")
	}

	trc.imageReturnedCounter.Add(req.Context(),
		1,
		metric.WithAttributes(attribute.Int("index", imageId)),
	)

	w.Header().Set("Content-Type", "image/png")
	w.Write(buf)
}
