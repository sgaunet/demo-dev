package controller

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type ImageServiceMock struct {
	mock.Mock
}

func (o *ImageServiceMock) ReadImage(imageFolder string, imageName string) ([]byte, error) {
	args := o.Called(imageFolder, imageName)
	if args.Get(1) == nil {
		return args.Get(0).([]byte), nil
	}
	return args.Get(0).([]byte), args.Get(1).(error)
}

func (o *ImageServiceMock) ReadImageNames(imagesFolder string) ([]string, error) {
	args := o.Called(imagesFolder)
	if args.Get(1) == nil {
		return args.Get(0).([]string), nil
	}
	return args.Get(0).([]string), args.Get(1).(error)
}

func TestTechRexRandomImageHandlerOneImage(t *testing.T) {
	viper.Set("img-path", "path")
	imageServiceMock := &ImageServiceMock{}
	imageServiceMock.On("ReadImageNames", "path").Return([]string{"anImageName.png"}, nil)
	imageServiceMock.On("ReadImage", "path", "anImageName.png").Return([]byte("an image"), nil)

	techRexController := NewTechRexController(imageServiceMock)
	techRexController.initImagesCache()

	req := httptest.NewRequest(http.MethodGet, "/", nil)
	res := httptest.NewRecorder()
	techRexController.techRexRandomImageHandler(res, req)

	assert.Equal(t, http.StatusOK, res.Code, "Status code should be 200")
	assert.Equal(t, []byte("an image"), res.Body.Bytes())
}

func TestTechRexRandomImageHandlerSeveralImages(t *testing.T) {
	viper.Set("img-path", "path")
	imageServiceMock := &ImageServiceMock{}
	imageServiceMock.On("ReadImageNames", "path").Return([]string{"anImageName.png", "anOtherImageName.png"}, nil)
	imageServiceMock.On("ReadImage", "path", mock.Anything).Return([]byte("an image"), nil)

	techRexController := NewTechRexController(imageServiceMock)
	techRexController.initImagesCache()

	req := httptest.NewRequest(http.MethodGet, "/", nil)
	res := httptest.NewRecorder()
	techRexController.techRexRandomImageHandler(res, req)

	assert.Equal(t, http.StatusOK, res.Code, "Status code should be 200")
	assert.Equal(t, []byte("an image"), res.Body.Bytes())
}
