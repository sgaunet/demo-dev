package service

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
)

type ImageServiceTestSuite struct {
	suite.Suite
	ImageService ImageServiceImpl
}

func (suite *ImageServiceTestSuite) SuiteSetup() {
	suite.ImageService = ImageServiceImpl{}
}

func (suite *ImageServiceTestSuite) TestReadImagesNames() {
	images, err := suite.ImageService.ReadImageNames("/workspace/go-dev/assets/")
	require.Nil(suite.T(), err, "err should be nil")
	require.NotEmpty(suite.T(), images, "there should be some images")
	assert.Equal(suite.T(), 11, len(images), "there shoud be 11 images")
}

func (suite *ImageServiceTestSuite) TestReadImage() {
	image, err := suite.ImageService.ReadImage("/workspace/go-dev/assets/", "coureur.png")
	require.Nil(suite.T(), err, "err should be nil")
	require.NotEmpty(suite.T(), image, "there should be some bytes in this image")
}

func TestImageServiceTestSuite(t *testing.T) {
	suite.Run(t, new(ImageServiceTestSuite))
}
