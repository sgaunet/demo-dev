package service

import (
	"os"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
)

type ImageService interface {
	ReadImageNames(imagesFolder string) ([]string, error)
	ReadImage(imageFolder string, imageName string) ([]byte, error)
}

type ImageServiceImpl struct {
}

func (is *ImageServiceImpl) ReadImageNames(imagesFolder string) ([]string, error) {
	log.Info().Msgf("Reading images from dir %s", imagesFolder)
	entries, err := os.ReadDir(imagesFolder)
	if err != nil {
		return nil, errors.Wrap(err, "error while initializing images")
	}

	log.Debug().Msg("Found " + strconv.Itoa(len(entries)) + " files ")
	images := make([]string, len(entries))
	for i, entry := range entries {
		images[i] = entry.Name()
	}

	return images, nil
}

func (is *ImageServiceImpl) ReadImage(imageFolder string, imageName string) ([]byte, error) {
	if strings.HasSuffix(imageFolder, "/") {
		return os.ReadFile(imageFolder + imageName)
	}
	return os.ReadFile(imageFolder + "/" + imageName)
}
